<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AnggotaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('anggota')->group(function () {
    Route::get('/', [AnggotaController::class, 'index']);
    Route::get('/add', [AnggotaController::class, 'add']);
    Route::post('/create', [AnggotaController::class, 'create']);
    Route::get('/edit/{id}', [AnggotaController::class, 'edit']);
    Route::post('/update', [AnggotaController::class, 'update']);
    Route::get('/delete/{id}', [AnggotaController::class, 'delete']);
});