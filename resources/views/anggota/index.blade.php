@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Anggota') }}</div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No Anggota</th>
                                <th>Nama Anggota</th>
                                <th>No HP</th>
                                <th>Gender</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($anggota as $data)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $data->no_anggota }}</td>
                                <td>{{ $data->nama_anggota }}</td>
                                <td>{{ $data->no_hp }}</td>
                                <td>{{ $data->gender}}</td>
                                <td>
                                    <a class="btn btn-warning" href="{{ url('/edit/$data->id') }}">Edit</a>
                                    <a class="btn btn-danger" href="{{ url('/delete/$data->id') }}">Hapus</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection